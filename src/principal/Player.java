package principal;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

/**
 *
 * @author Luiz Quintel
 */
public class Player extends Base
{

    private int vida = 3;

    private Image playerImage;

    public Player(int larguraScreen, int alturaScreen)
    {
        super(larguraScreen, alturaScreen);

        try
        {
            playerImage = ImageIO.read(new File("./dados/PLAYER.png"));
        }
        catch (IOException ex)
        {
            Logger.getLogger(Player.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void desenhar(Graphics g)
    {
        //ImageIcon img = new ImageIcon("Imagem/P.png");
        g.setColor(getCor());
        //g.fillOval(getX(), getY(), getLargura(), getAltura());
        g.drawImage(playerImage, getX(), getY(), 60, 30, null);
    }

    @Override
    public void configura()
    {
        setX((getLargura() / 2));
        setY(getAlturaScreen() / 2 - (getAltura() / 2));
        retangulo = new Rectangle(getX(), getY(), getLargura(), getAltura());
    }

    @Override
    public void move(Movimento movimento)
    {

        if (movimento == Movimento.SUBIR && getY() - 35 > 0)
            setY(getY() - 10);
        else if (movimento == Movimento.DESCER && getY() + getAltura() + 14 < getAlturaScreen())
            setY(getY() + 10);

        retangulo.setBounds(getX(), getY(), getLargura(), getAltura());
    }

    @Override
    public void configura(int numero)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean verificaLimite()
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public int getVidas()
    {
        return vida;
    }

    public void removeVida()
    {
        this.vida--;
    }

}
