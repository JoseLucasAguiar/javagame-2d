/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package principal;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import javax.swing.ImageIcon;


public abstract class Base {
    private int x = 0;
    private int y = 0;
    private Color cor = Color.BLACK;
    private Color borda = Color.RED;
    private int largura=30;
    private int altura=30;
    private int larguraScreen;
    private int alturaScreen;
    protected Rectangle retangulo;
    private int incY;
    private int incX;
    
    public Base(int larguraScreen, int alturaScreen){
        this.larguraScreen = larguraScreen;
        this.alturaScreen = alturaScreen;
    }
    
    public int getLarguraScreen() {
        return larguraScreen;
    }

    public void setLarguraScreen(int larguraScreen) {
        this.larguraScreen = larguraScreen;
    }

    public int getAlturaScreen() {
        return alturaScreen;
    }

    public void setAlturaScreen(int alturaScreen) {
        this.alturaScreen = alturaScreen;
    }

    public int getLargura() {
        return largura;
    }

    public int getAltura() {
        return altura;
    }
    
    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }
   
    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }


    public Color getCor() {
        return cor;
    }

    public void setCor(Color cor) {
        this.cor = cor;
    }

    public void setLargura(int largura) {
        this.largura = largura;
    }
    
    public void setAltura(int altura) {
        this.altura = altura;
    }
    
    public abstract void desenhar(Graphics g);
    public abstract void move(Movimento movimento);
    public abstract void configura();
    public abstract void configura(int numero);
    public abstract boolean verificaLimite();

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 47 * hash + this.x;
        hash = 47 * hash + this.y;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Base other = (Base) obj;
        if (this.x != other.x) {
            return false;
        }
        return this.y == other.y;
    }

    

}
