/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package principal;

/**
 *
 * @author Luiz Henrique
 */
public enum Movimento {
    
    SUBIR(1), DESCER(2), PARAR(3), DIREITA(4), ESQUERDA(5);
     
    private final int valor;
    
    Movimento(int valorOpcao){
        valor = valorOpcao;
    }
    public int getValor(){
        return valor;
    }
}
