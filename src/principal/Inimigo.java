/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package principal;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

/**
 *
 * @author Luiz Quintel
 */
public class Inimigo extends Base{

    private int velocidade = 3;
    private boolean intersect = false;

    public boolean isIntersect() {
        return intersect;
    }

    public void setIntersect(boolean intersect) {
        this.intersect = intersect;
    }
    
    public Inimigo(int larguraScreen, int alturaScreen) {
        super(larguraScreen, alturaScreen);
    }

    @Override
    public void desenhar(Graphics g) {
        
        g.setColor(Color.RED);
        g.fillOval(getX(), getY(), getLargura(), getAltura());
    }

    @Override
    public void move(Movimento movimento) {
        setX(getX() - velocidade);
        retangulo.setBounds(getX(), getY(), getLargura(), getAltura());
    }

    @Override
    public void configura() {}

    @Override
    public void configura(int numero) {
        setY(numero);
        setX(getLarguraScreen() + getLargura() * 10);
        retangulo = new Rectangle(getX(), getY(), getLargura(), getAltura());
        intersect = false;
    }

    public int getVelocidade() {
        return velocidade;
    }

    public void setVelocidade(int velocidade) {
        this.velocidade = velocidade;
    }

    @Override
    public boolean verificaLimite() {
        return (getX() + getLargura() < 0);
    }
    
    public void reconfigura(int valor){
        configura(valor);
        if(velocidade < 30)
            velocidade+=3;
    }
    
}
