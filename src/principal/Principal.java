/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package principal;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author Luiz Quintel
 */
public class Principal extends javax.swing.JFrame implements Runnable{

    
    private Movimento playerAction;
    private boolean intersect = false;
    
    public Principal() {
        initComponents(); 
        createBufferStrategy(2);
        setTitle("Luiz Henrique Ferreira Quintel - 160066");
        setResizable(false);
        Thread t = new Thread(this);
        t.start();
    }   

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                formMousePressed(evt);
            }
        });
        addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                formKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                formKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                formKeyTyped(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMousePressed
        // TODO add your handling code here:
    }//GEN-LAST:event_formMousePressed

    private void formKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyPressed

        if(evt.getKeyCode() == KeyEvent.VK_UP)  
            playerAction = Movimento.SUBIR;
        else if(evt.getKeyCode() == KeyEvent.VK_DOWN)
            playerAction = Movimento.DESCER;
    }//GEN-LAST:event_formKeyPressed

    private void formKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyReleased
        playerAction = Movimento.PARAR;
    }//GEN-LAST:event_formKeyReleased

    private void formKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyTyped
        
    }//GEN-LAST:event_formKeyTyped

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Principal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Principal().setVisible(true);
            }
        });
    }

    @Override
    public void run() {
        Random r = new Random(System.currentTimeMillis());
        
        Player p = new Player(getWidth(),getHeight());
        List<Inimigo> inimigos = new ArrayList();
        Graphics g = getBufferStrategy().getDrawGraphics();
        p.configura();
        boolean status = true;
        
        
        for(int i = 0; i < 10; i++){
            Inimigo inimigo = new Inimigo(getWidth(), getHeight());
            int y = r.nextInt(inimigo.getAlturaScreen() - inimigo.getAltura() + 1) + inimigo.getAltura();
            inimigo.configura(y);
            inimigo.setVelocidade(r.nextInt(10) + 1);
            inimigos.add(inimigo); 
        }
        
        while(status)
        {
           
           g.setColor(Color.WHITE);
           g.fillRect(0,0,getWidth(),getHeight());
           
           g.setColor(Color.BLACK);
            g.setFont(new Font("TimesRoman", Font.PLAIN, 25));
            g.drawString("Life: " + p.getVidas(), 30, 55);
          
            if(playerAction == Movimento.SUBIR)  
                p.move(Movimento.SUBIR);
            else if(playerAction == Movimento.DESCER)
                p.move(Movimento.DESCER);
            
            for(Inimigo inimigo : inimigos){
                inimigo.move(playerAction);
                
                if(inimigo.verificaLimite())
                    inimigo.reconfigura(r.nextInt(inimigo.getAlturaScreen() - inimigo.getAltura() + 1) + inimigo.getAltura());
                inimigo.desenhar(g);
                
                if(inimigo.retangulo.intersects(p.retangulo) && !inimigo.isIntersect()){
                    inimigo.setIntersect(true);
                    p.removeVida();
                }
                
                if(p.getVidas() == 0){
                    status = false;
                }
            }
            
            p.desenhar(g);
            getBufferStrategy().show();
            
            try {
                Thread.sleep(50);
            } catch (InterruptedException ex) {}
  
}
        
        g.setColor(Color.BLACK);
        g.fillRect(0,0,getWidth(),getHeight());
        g.setColor(Color.RED);
        g.setFont(new Font("TimesRoman", Font.PLAIN, 50));
        g.drawString("GAME OVER", 50, getHeight() / 2);
        getBufferStrategy().show();
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
}
